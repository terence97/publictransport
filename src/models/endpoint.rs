use gtk::subclass::prelude::*;
use hafas_client::Endpoint;
use std::cell::RefCell;

pub mod imp {
    use super::*;

    #[derive(Debug)]
    pub struct HafasEndpoint {
        pub endpoint: RefCell<Endpoint>,
    }

    // necessary because hafas-client does't implement the Default trait for Endpoint
    impl std::default::Default for HafasEndpoint {
        fn default() -> Self {
            HafasEndpoint {
                endpoint: RefCell::new(Endpoint::new(0).unwrap()), // use db endpoint as default
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for HafasEndpoint {
        const NAME: &'static str = "HafasEndpoint";
        type Type = super::HafasEndpoint;

        fn new() -> Self {
            Self {
                endpoint: RefCell::new(Endpoint::new(0).unwrap()),
            }
        }
    }

    impl ObjectImpl for HafasEndpoint {}
}

glib::wrapper! {
    pub struct HafasEndpoint(ObjectSubclass<imp::HafasEndpoint>);
}

impl HafasEndpoint {
    pub fn new(endpoint: &Endpoint) -> Self {
        let hafas_endpoint: HafasEndpoint =
            glib::Object::new(&[]).expect("Failed to create HafasEndpoint");
        *hafas_endpoint.imp().endpoint.borrow_mut() = endpoint.clone();
        hafas_endpoint
    }

    pub fn get(&self) -> Endpoint {
        let endpoint_ref = self.imp().endpoint.borrow();
        endpoint_ref.clone()
    }
}

impl std::default::Default for HafasEndpoint {
    fn default() -> Self {
        glib::Object::new(&[]).expect("Failed to create HafasEndpoint")
    }
}
