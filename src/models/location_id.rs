use gtk::subclass::prelude::*;
use std::cell::Cell;

pub mod imp {
    use super::*;

    #[derive(Debug)]
    pub struct LocationId {
        pub location_id: Cell<Option<i64>>,
    }

    impl std::default::Default for LocationId {
        fn default() -> Self {
            LocationId {
                location_id: Cell::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for LocationId {
        const NAME: &'static str = "LocationId";
        type Type = super::LocationId;

        fn new() -> Self {
            Self {
                location_id: Cell::new(None),
            }
        }
    }

    impl ObjectImpl for LocationId {}
}

glib::wrapper! {
    pub struct LocationId(ObjectSubclass<imp::LocationId>);
}

impl LocationId {
    pub fn new(id: Option<i64>) -> Self {
        let location_id: LocationId = glib::Object::new(&[]).expect("Failed to create LocationId");
        location_id.imp().location_id.set(id);
        location_id
    }

    pub fn get(&self) -> Option<i64> {
        self.imp().location_id.get()
    }
}

impl std::default::Default for LocationId {
    fn default() -> Self {
        glib::Object::new(&[]).expect("Failed to create LocationId")
    }
}
