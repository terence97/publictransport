use gtk::subclass::prelude::*;
use hafas_client::Journey;
use std::cell::RefCell;

pub mod imp {
    use super::*;

    #[derive(Debug)]
    pub struct HafasJourney {
        pub journey: RefCell<Option<Journey>>,
    }

    // necessary because hafas-client does't implement the Default trait for Journey
    impl std::default::Default for HafasJourney {
        fn default() -> Self {
            HafasJourney {
                journey: RefCell::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for HafasJourney {
        const NAME: &'static str = "HafasJourney";
        type Type = super::HafasJourney;

        fn new() -> Self {
            Self {
                journey: RefCell::new(None),
            }
        }
    }

    impl ObjectImpl for HafasJourney {}
}

glib::wrapper! {
    pub struct HafasJourney(ObjectSubclass<imp::HafasJourney>);
}

impl HafasJourney {
    pub fn new(journey: &Journey) -> Self {
        let hafas_journey: HafasJourney =
            glib::Object::new(&[]).expect("Failed to create HafasJourney");
        *hafas_journey.imp().journey.borrow_mut() = Some(journey.clone());
        hafas_journey
    }

    pub fn get(&self) -> Option<Journey> {
        let journey_ref = self.imp().journey.borrow();
        journey_ref.clone()
    }
}

impl std::default::Default for HafasJourney {
    fn default() -> Self {
        glib::Object::new(&[]).expect("Failed to create HafasJourney")
    }
}
