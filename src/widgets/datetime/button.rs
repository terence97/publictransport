use crate::models::endpoint::HafasEndpoint;
use gettextrs::gettext;
use glib::{Object, Value};
use glib::{ParamFlags, ParamSpec, ParamSpecObject};
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use once_cell::sync::Lazy;
use std::cell::RefCell;

mod imp {
    use super::*;

    #[derive(Default)]
    pub struct DatetimeButton {
        pub endpoint: RefCell<HafasEndpoint>,
    }

    // The central trait for subclassing a GObject
    #[glib::object_subclass]
    impl ObjectSubclass for DatetimeButton {
        const NAME: &'static str = "DatetimeButton";
        type Type = super::DatetimeButton;
        type ParentType = gtk::Button;
    }

    // Trait shared by all GObjects
    impl ObjectImpl for DatetimeButton {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![ParamSpecObject::new(
                    // Name
                    "endpoint",
                    // Nickname
                    "endpoint",
                    // Short description
                    "endpoint",
                    // Default value
                    HafasEndpoint::static_type(),
                    // The property can be read and written to
                    ParamFlags::READWRITE,
                )]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _obj: &Self::Type, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "endpoint" => {
                    let endpoint = value
                        .get()
                        .expect("The value needs to be of type `HafasEndpoint`.");
                    self.endpoint.replace(endpoint);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "endpoint" => self.endpoint.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            obj.connect_notify_local(Some("endpoint"), move |datetime_button, _| {
                let hafas_endpoint = datetime_button.property::<HafasEndpoint>("endpoint");
                let datetime = hafas_endpoint.get().journeys_config.datetime;
                match datetime.lock().unwrap().clone() {
                    Some(dt) => {
                        datetime_button.set_label(&dt.format("%d.%m.%Y, %H:%M").to_string())
                    }
                    None => datetime_button.set_label(&"Now".to_string()),
                };
            });
        }
    }

    // Trait shared by all widgets
    impl WidgetImpl for DatetimeButton {}

    // Trait shared by all buttons
    impl ButtonImpl for DatetimeButton {
        fn clicked(&self, button: &Self::Type) {
            // self.number.set(self.number.get() + 1);
            // button.set_label(&self.number.get().to_string())

            let _ = button.activate_action(
                "win.show-leaflet-page",
                Some(&gettext("date_time").to_variant()),
            );
        }
    }
}

glib::wrapper! {
    pub struct DatetimeButton(ObjectSubclass<imp::DatetimeButton>)
    @extends gtk::Button, gtk::Widget,
    @implements gtk::Accessible, gtk::Actionable, gtk::Buildable, gtk::ConstraintTarget;
}

impl DatetimeButton {
    pub fn new() -> Self {
        Object::new(&[]).expect("Failed to create `DatetimeButton`.")
    }

    pub fn with_label(label: &str) -> Self {
        Object::new(&[("label", &label)]).expect("Failed to create `DatetimeButton`.")
    }
}

impl std::default::Default for DatetimeButton {
    fn default() -> Self {
        glib::Object::new(&[]).expect("Failed to create DatetimeButton")
    }
}
