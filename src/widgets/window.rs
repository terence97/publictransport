use glib::FromVariant;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib, CompositeTemplate};

use crate::models::{endpoint::HafasEndpoint, journey::HafasJourney};
use crate::widgets::JourneysMainView;

mod imp {
    use super::*;
    use adw::subclass::application_window::AdwApplicationWindowImpl;
    use glib::{BindingFlags, ParamFlags, ParamSpec, ParamSpecObject, Value};
    use once_cell::sync::Lazy;
    use std::cell::RefCell;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/window.ui")]
    pub struct PublictransportWindow {
        // Template widgets
        #[template_child]
        pub header_bar: TemplateChild<gtk::HeaderBar>,
        #[template_child]
        pub main_view: TemplateChild<JourneysMainView>,
        #[template_child]
        pub toast: TemplateChild<adw::ToastOverlay>,
        #[template_child]
        pub leaflet: TemplateChild<adw::Leaflet>,
        #[template_child]
        pub details_view: TemplateChild<crate::widgets::journeys::detailsview::JourneysDetailsView>,
        #[template_child]
        pub datetime_view: TemplateChild<crate::widgets::datetime::view::DateTimeView>,
        pub endpoint: RefCell<HafasEndpoint>,
        pub details_journey: RefCell<HafasJourney>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for PublictransportWindow {
        const NAME: &'static str = "PublictransportWindow";
        type Type = super::PublictransportWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);

            klass.install_action("win.add-toast", Some("s"), |obj, _, message| {
                if let Some(message) = message.and_then(String::from_variant) {
                    let toast = adw::Toast::new(&message);
                    let toast_overlay = obj.imp().toast.get();
                    toast_overlay.add_toast(&toast);
                }
            });

            klass.install_action("win.show-leaflet-page", Some("s"), |obj, _, message| {
                if let Some(message) = message.and_then(String::from_variant) {
                    obj.imp().leaflet.get().set_visible_child_name(&message);
                }
            });

            klass.install_action("win.show-main-leaflet", None, |obj, _, _| {
                obj.imp().leaflet.get().set_visible_child_name("main");
            });

            klass.install_action("win.refresh-details-journey", None, |obj, _, _| {
                let endpoint = obj.imp().endpoint.borrow().get();
                let journey = obj.imp().details_journey.borrow().get();
                obj.imp()
                    .details_view
                    .get()
                    .refresh_journey(&endpoint, &journey.unwrap());
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for PublictransportWindow {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::new(
                        // Name
                        "endpoint",
                        // Nickname
                        "endpoint",
                        // Short description
                        "endpoint",
                        // Default value
                        HafasEndpoint::static_type(),
                        // The property can be read and written to
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecObject::new(
                        "details-journey",
                        "details-journey",
                        "details-journey",
                        HafasJourney::static_type(),
                        ParamFlags::READWRITE,
                    ),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _obj: &Self::Type, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "endpoint" => {
                    let endpoint = value
                        .get()
                        .expect("The value needs to be of type `HafasEndpoint`.");
                    self.endpoint.replace(endpoint);
                }
                "details-journey" => {
                    let details_journey = value
                        .get()
                        .expect("The value needs to be of type `HafasJourney`.");
                    self.details_journey.replace(details_journey);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "endpoint" => self.endpoint.borrow().to_value(),
                "details-journey" => self.details_journey.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            obj.bind_property("endpoint", &obj.imp().main_view.get(), "endpoint")
                .flags(BindingFlags::BIDIRECTIONAL)
                .build();

            obj.bind_property(
                "endpoint",
                &obj.imp()
                    .main_view
                    .get()
                    .imp()
                    .departure_place
                    .get()
                    .imp()
                    .popover,
                "endpoint",
            )
            .flags(BindingFlags::DEFAULT)
            .build();

            obj.bind_property(
                "endpoint",
                &obj.imp()
                    .main_view
                    .get()
                    .imp()
                    .arrival_place
                    .get()
                    .imp()
                    .popover,
                "endpoint",
            )
            .flags(BindingFlags::DEFAULT)
            .build();

            obj.bind_property(
                "endpoint",
                &obj.imp().main_view.get().imp().date_time.get(),
                "endpoint",
            )
            .flags(BindingFlags::BIDIRECTIONAL)
            .build();

            obj.bind_property("endpoint", &obj.imp().datetime_view.get(), "endpoint")
                .flags(BindingFlags::BIDIRECTIONAL)
                .build();

            obj.imp()
                .main_view
                .imp()
                .journeys_view
                .bind_property("details-journey", obj, "details-journey")
                .flags(BindingFlags::DEFAULT)
                .build();

            obj.connect_notify_local(Some("details-journey"), move |window, _| {
                let hafas_journey = &*window.imp().details_journey.borrow();
                let journey = hafas_journey.get().unwrap();
                window.imp().details_view.set_journey_details(&journey);
                window
                    .imp()
                    .leaflet
                    .get()
                    .set_visible_child_name("journey_details");
            });
        }
    }
    impl WidgetImpl for PublictransportWindow {}
    impl WindowImpl for PublictransportWindow {}
    impl ApplicationWindowImpl for PublictransportWindow {}
    impl AdwApplicationWindowImpl for PublictransportWindow {}
}

glib::wrapper! {
    pub struct PublictransportWindow(ObjectSubclass<imp::PublictransportWindow>)
    @extends gtk::Widget, gtk::Window, adw::ApplicationWindow, gtk::ApplicationWindow,
    @implements gio::ActionGroup, gio::ActionMap;
}

impl PublictransportWindow {
    pub fn new<P: glib::IsA<gtk::Application>>(application: &P) -> Self {
        let window: PublictransportWindow = glib::Object::new(&[("application", application)])
            .expect("Failed to create PublictransportWindow");

        // set up shortcuts
        let builder =
            gtk::Builder::from_resource("/com/gitlab/maevemi/publictransport/shortcuts.ui");
        gtk_macros::get_widget!(builder, gtk::ShortcutsWindow, help_overlay);
        window.set_help_overlay(Some(&help_overlay));

        window
    }
}
