use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};
use hafas_client::Leg;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/journeys_leg_row.ui")]
    pub struct LegRow {
        #[template_child]
        pub planned_departure: TemplateChild<gtk::Label>,
        #[template_child]
        pub departure_delay: TemplateChild<gtk::Label>,
        #[template_child]
        pub origin: TemplateChild<gtk::Label>,
        #[template_child]
        pub departure_track: TemplateChild<gtk::Label>,
        #[template_child]
        pub line: TemplateChild<gtk::Box>,
        #[template_child]
        pub line_destination: TemplateChild<gtk::Label>,
        #[template_child]
        pub duration: TemplateChild<gtk::Label>,
        #[template_child]
        pub planned_arrival: TemplateChild<gtk::Label>,
        #[template_child]
        pub arrival_delay: TemplateChild<gtk::Label>,
        #[template_child]
        pub destination: TemplateChild<gtk::Label>,
        #[template_child]
        pub arrival_track: TemplateChild<gtk::Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for LegRow {
        const NAME: &'static str = "JourneyLegRow";
        type Type = super::LegRow;
        type ParentType = gtk::ListBoxRow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for LegRow {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);
        }
    }
    impl WidgetImpl for LegRow {}
    impl ListBoxRowImpl for LegRow {}
}

glib::wrapper! {
    pub struct LegRow(ObjectSubclass<imp::LegRow>)
        @extends gtk::Widget, gtk::ListBoxRow,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Actionable;
}

impl LegRow {
    pub fn new(leg: &Leg) -> Self {
        let leg_row: LegRow = glib::Object::new(&[]).expect("Failed to create LegRow");

        leg_row.fill(leg);
        leg_row
    }

    pub fn fill(&self, leg: &Leg) -> &Self {
        let dep_planned = &leg.plannedDeparture.unwrap();
        let arr_planned = &leg.plannedArrival.unwrap();

        // TODO create help function (can be used by journeyRow as well)
        let str_dep_delay = match &leg.departure {
            Some(d) => {
                let mut str_dep_delay = String::from("+");
                str_dep_delay.push_str(
                    &d.signed_duration_since(*dep_planned)
                        .num_minutes()
                        .to_string(),
                );
                str_dep_delay
            }
            None => String::from(""),
        };
        let str_arr_delay = match &leg.arrival {
            Some(d) => {
                let mut str_arr_delay = String::from("+");
                str_arr_delay.push_str(
                    &d.signed_duration_since(*arr_planned)
                        .num_minutes()
                        .to_string(),
                );
                str_arr_delay
            }
            None => String::from(""),
        };

        // TODO create help function (can be used by journeyRow as well)
        let duration_str = match &leg.plannedDeparture {
            Some(depa) => {
                let duration_str = match &leg.plannedArrival {
                    Some(arri) => {
                        let duration = arri.signed_duration_since(*depa);
                        let minutes = duration.num_minutes() % 60;
                        let mut duration_str = String::from(" ");
                        duration_str.push_str(&duration.num_hours().to_string());
                        duration_str.push(':');
                        if minutes < 10 {
                            duration_str.push('0');
                        }
                        duration_str.push_str(&minutes.to_string());
                        duration_str.push_str(&" h".to_string());
                        duration_str
                    }
                    _ => "".to_string(),
                };
                duration_str
            }
            _ => "".to_string(),
        };

        // TODO create help function (can be used by journeyRow as well)
        let dep_track = match &leg.departurePlatform {
            Some(d) => {
                let mut str = "Pl. ".to_string();
                str.push_str(&d);
                str
            }
            None => "".to_string(),
        };
        let arr_track = match &leg.arrivalPlatform {
            Some(d) => {
                let mut str = "Pl. ".to_string();
                str.push_str(&d);
                str
            }
            None => "".to_string(),
        };

        let imp = self.imp();

        let line_box = gtk::Box::new(gtk::Orientation::Horizontal, 0);
        line_box.set_halign(gtk::Align::Center);
        line_box.set_valign(gtk::Align::Center);
        match &leg.line {
            Some(l) => {
                let mut line_str = " ".to_string();
                line_str.push_str(&l);
                line_str.push_str(&" ".to_string());

                line_box.append(&gtk::Label::new(Some(&line_str.to_string())));
            }
            None => {
                let image = gtk::Image::new();
                image.set_icon_name(Some("walking-symbolic"));
                image.set_margin_top(3);
                image.set_margin_bottom(3);
                line_box.append(&image);
            }
        }

        let dark = adw::StyleManager::default().is_dark();
        if dark {
            line_box.add_css_class("product-background-dark")
        } else {
            line_box.add_css_class("product-background")
        }

        imp.line.get().append(&line_box);

        imp.planned_departure
            .get()
            .set_markup(&dep_planned.format("%H:%M").to_string());
        imp.departure_delay.get().set_markup(&str_dep_delay);
        imp.origin.get().set_markup(&leg.origin.name.clone());
        imp.departure_track.get().set_markup(&dep_track);
        imp.line_destination
            .get()
            .set_markup(&leg.direction.as_ref().unwrap_or(&"".to_string()).clone());
        imp.duration.get().set_markup(&duration_str);
        imp.planned_arrival
            .get()
            .set_markup(&arr_planned.format("%H:%M").to_string());
        imp.arrival_delay.get().set_markup(&str_arr_delay);
        imp.destination
            .get()
            .set_markup(&leg.destination.name.clone());
        imp.arrival_track.get().set_markup(&arr_track);

        self
    }
}
