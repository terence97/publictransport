mod leg_row;

use glib::clone;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};
use hafas_client::{BoxError, Endpoint, Journey};
use log::{error, warn};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/journeys_detailsview.ui")]
    pub struct JourneysDetailsView {
        #[template_child]
        pub refresh_journey_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub refresh_journey_spinner: TemplateChild<gtk::Spinner>,
        #[template_child]
        pub journey_legs_listbox: TemplateChild<gtk::ListBox>,
        #[template_child]
        pub journey_duration: TemplateChild<gtk::Label>,
        #[template_child]
        pub toast: TemplateChild<adw::ToastOverlay>,
        #[template_child]
        pub title: TemplateChild<gtk::Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for JourneysDetailsView {
        const NAME: &'static str = "JourneysDetailsView";
        type Type = super::JourneysDetailsView;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for JourneysDetailsView {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);
        }
    }
    impl WidgetImpl for JourneysDetailsView {}
    impl BoxImpl for JourneysDetailsView {}
}

glib::wrapper! {
    pub struct JourneysDetailsView(ObjectSubclass<imp::JourneysDetailsView>)
        @extends gtk::Widget, gtk::Box,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Orientable;
}

impl JourneysDetailsView {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create JourneysDetailsView")
    }

    pub fn set_journey_details(&self, journey: &Journey) {
        let origin = journey.legs[0].origin.name.clone();
        let destination = &journey
            .legs
            .last()
            .expect("Connection part not found")
            .destination
            .name;
        let mut origin_destination = origin;
        origin_destination.push_str(&" -> ".to_string());
        origin_destination.push_str(&destination);

        // TODO ceate util
        let dep_planned = &journey.legs[0].plannedDeparture.unwrap();
        let arr_planned = &journey
            .legs
            .last()
            .expect("Connection part not found")
            .plannedArrival
            .unwrap();
        let duration = &arr_planned.signed_duration_since(*dep_planned);
        let mut j_duration = "full journey duration: ".to_string();
        let minutes = duration.num_minutes() % 60;
        j_duration.push_str(&duration.num_hours().to_string());
        j_duration.push(':');
        if minutes < 10 {
            j_duration.push('0');
        }
        j_duration.push_str(&minutes.to_string());
        j_duration.push_str(&" h".to_string());

        let imp = self.imp();

        self.clear_details_list();
        for leg in journey.legs.iter() {
            imp.journey_legs_listbox
                .get()
                .append(&leg_row::LegRow::new(leg));
        }
        imp.journey_duration.get().set_markup(&j_duration);
        imp.title.get().set_markup(&origin_destination);
    }

    pub fn refresh_journey(&self, endpoint: &Endpoint, journey: &Journey) {
        self.imp().refresh_journey_spinner.get().set_visible(true);
        self.imp().refresh_journey_button.get().set_visible(false);

        let journey_handle: tokio::task::JoinHandle<Result<Journey, BoxError>> = crate::RUNTIME
            .spawn(clone!(@strong journey, @strong endpoint => async move {
                let journey = endpoint.refresh_journey(&journey).await;
                // std::thread::sleep(std::time::Duration::from_secs(2));
                journey
            }));

        gtk_macros::spawn!(clone!(
        @weak self as details_view
        => async move {
            match journey_handle.await {
                Ok(journey_handle_inner) => match journey_handle_inner {
                    Ok(journey) => {
                        details_view.imp().refresh_journey_spinner.get().set_visible(false);
                        details_view.imp().refresh_journey_button.get().set_visible(true);
                        details_view.clear_details_list();
                        details_view.set_journey_details(&journey);
                    },
                    Err(e) => {
                        warn!("Error refreshing journey: {:#?}", e);
                        details_view.imp().refresh_journey_spinner.get().set_visible(false);
                        details_view.imp().refresh_journey_button.get().set_visible(true);
                        let toast = adw::Toast::new("Error refreshing journey!");
                        let toast_overlay = details_view.imp().toast.get();
                        toast_overlay.add_toast(&toast);
                    },
                },
                Err(e) => {
                    let toast = adw::Toast::new("Internal Error refreshing journey!");
                    let toast_overlay = details_view.imp().toast.get();
                    toast_overlay.add_toast(&toast);
                    error!("Handle error: {:#?}", e);
                    details_view.imp().refresh_journey_spinner.get().set_visible(false);
                    details_view.imp().refresh_journey_button.get().set_visible(true);
                },
            };
        }));
    }

    pub fn clear_details_list(&self) {
        // TODO: move to utils and use to clear location list as well
        loop {
            let row = match self.imp().journey_legs_listbox.get().row_at_index(0) {
                Some(r) => r,
                None => break,
            };
            self.imp().journey_legs_listbox.get().remove(&row);
        }
    }
}
