use gettextrs::gettext;
use glib::clone;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};
use hafas_client::{BoxError, Journeys};
use log::{error, warn};

use crate::models::{endpoint::HafasEndpoint, journey::HafasJourney};
use crate::widgets::journeys::journey_row::JourneyRow;

mod imp {
    use super::*;
    use glib::{ParamFlags, ParamSpec, ParamSpecObject, Value};
    use once_cell::sync::Lazy;
    use std::cell::RefCell;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/journeys_view.ui")]
    pub struct JourneysView {
        #[template_child]
        pub journeys_listbox: TemplateChild<gtk::ListBox>,
        #[template_child]
        pub journeys_search_spinner: TemplateChild<gtk::Box>,
        #[template_child]
        pub earlier_journeys: TemplateChild<gtk::Button>,
        #[template_child]
        pub later_journeys: TemplateChild<gtk::Button>,
        #[template_child]
        pub earlier_journeys_spinner: TemplateChild<gtk::Spinner>,
        #[template_child]
        pub later_journeys_spinner: TemplateChild<gtk::Spinner>,
        pub endpoint: RefCell<HafasEndpoint>,
        pub details_journey: RefCell<HafasJourney>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for JourneysView {
        const NAME: &'static str = "JourneysView";
        type Type = super::JourneysView;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for JourneysView {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::new(
                        "endpoint",
                        "endpoint",
                        "endpoint",
                        HafasEndpoint::static_type(),
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecObject::new(
                        "details-journey",
                        "details-journey",
                        "details-journey",
                        HafasJourney::static_type(),
                        ParamFlags::READWRITE,
                    ),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _obj: &Self::Type, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "endpoint" => {
                    let endpoint = value
                        .get()
                        .expect("The value needs to be of type `HafasEndpoint`.");
                    self.endpoint.replace(endpoint);
                }
                "details-journey" => {
                    let details_journey = value
                        .get()
                        .expect("The value needs to be of type `HafasJourney`.");
                    self.details_journey.replace(details_journey);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "endpoint" => self.endpoint.borrow().to_value(),
                "details-journey" => self.details_journey.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self, obj: &Self::Type) {
            obj.setup_signals();

            self.parent_constructed(obj);
        }
    }
    impl WidgetImpl for JourneysView {}
    impl BoxImpl for JourneysView {}
}

glib::wrapper! {
    pub struct JourneysView(ObjectSubclass<imp::JourneysView>)
        @extends gtk::Widget, gtk::Box,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Orientable;
}

impl JourneysView {
    pub fn new() -> Self {
        let journeys_view: JourneysView =
            glib::Object::new(&[]).expect("Failed to create JourneysView");

        journeys_view
    }

    pub fn setup_signals(&self) {
        self.connect_notify_local(Some("endpoint"), move |journeys_view, _| {
            let endpoint = journeys_view.imp().endpoint.borrow().get();

            if endpoint.get_departure_id().is_some() && endpoint.get_arrival_id().is_some() {
                journeys_view.journeys();
            }
        });

        self.imp().earlier_journeys.connect_clicked(clone!(
            @weak self as journeys_view
            => move |_| {
            journeys_view.earlier_journeys();
        }));

        self.imp().later_journeys.connect_clicked(clone!(
            @weak self as journeys_view
            => move |_| {
            journeys_view.later_journeys();
        }));

        self.imp().journeys_listbox.connect_row_activated(
            clone!(@weak self as journeys_view => move |_list, row| {
                let row = row.downcast_ref::<JourneyRow>().unwrap();
                let hafas_journey = &*row.imp().journey.borrow();
                journeys_view.set_property("details-journey", hafas_journey);
            }),
        );
    }

    pub fn journeys(&self) {
        self.clear_journeys_list();
        self.imp().earlier_journeys.get().set_visible(false);
        self.imp().later_journeys.get().set_visible(false);
        self.imp().journeys_listbox.get().set_visible(false);
        self.imp().journeys_search_spinner.get().set_visible(true);

        let hafas_endpoint = &*self.imp().endpoint.borrow();
        let endpoint = hafas_endpoint.get().clone();

        let journeys_handle: tokio::task::JoinHandle<Result<Journeys, BoxError>> = crate::RUNTIME
            .spawn(async move {
                let journeys = endpoint.get_journeys().await;
                // std::thread::sleep(std::time::Duration::from_secs(2));
                journeys
            });

        gtk_macros::spawn!(clone!(
        @weak self as journeys_view
        => async move {
            match journeys_handle.await {
                Ok(journeys_handle_inner) => match journeys_handle_inner {
                    Ok(journeys) => {
                        journeys_view.clear_journeys_list();
                        journeys_view.imp().earlier_journeys.get().set_visible(true);
                        journeys_view.imp().later_journeys.get().set_visible(true);
                        journeys_view.imp().journeys_search_spinner.get().set_visible(false);
                        journeys_view.imp().journeys_listbox.get().set_visible(true);
                        let _ = journeys_view.activate_action("win.show-journeys-refresh", None);
                        for journey in journeys.journeys.iter() {
                            let row = JourneyRow::new(&journey);
                            journeys_view.imp().journeys_listbox.get().append(&row);
                        }
                    },
                    Err(e) => {
                        warn!("Error getting journeys: {:#?}", e);
                        journeys_view.imp().journeys_search_spinner.get().set_visible(false);
                        let _ = journeys_view.activate_action("win.hide-journeys-refresh", None);
                        journeys_view.show_toast("No Journeys found");
                    },
                },
                Err(e) => {
                    journeys_view.show_toast("internal Error getting Journeys");
                    error!("Handle error: {:#?}", e);
                    journeys_view.imp().journeys_search_spinner.get().set_visible(false);
                    let _ = journeys_view.activate_action("win.hide-journeys-refresh", None);
                },
            };
        }));
    }

    pub fn earlier_journeys(&self) {
        self.imp().earlier_journeys_spinner.get().set_visible(true);
        self.imp().earlier_journeys.get().set_visible(false);

        let hafas_endpoint = &*self.imp().endpoint.borrow();
        let endpoint = hafas_endpoint.get().clone();

        let journeys_handle: tokio::task::JoinHandle<Result<Journeys, BoxError>> = crate::RUNTIME
            .spawn(async move {
                let journeys = endpoint.get_earlier_journeys().await;
                // std::thread::sleep(std::time::Duration::from_secs(2));
                journeys
            });

        gtk_macros::spawn!(clone!(
        @weak self as journeys_view
        => async move {
            match journeys_handle.await {
                Ok(journeys_handle_inner) => match journeys_handle_inner {
                    Ok(mut journeys) => {
                        journeys_view.imp().earlier_journeys.get().set_visible(true);
                        journeys_view.imp().earlier_journeys_spinner.get().set_visible(false);
                        journeys.journeys.reverse();
                        for journey in journeys.journeys.iter() {
                            let row = JourneyRow::new(&journey);
                            journeys_view.imp().journeys_listbox.get().prepend(&row);
                        }
                    },
                    Err(e) => {
                        warn!("Error getting earlier journeys: {:#?}", e);
                        journeys_view.imp().earlier_journeys_spinner.get().set_visible(false);
                        journeys_view.imp().earlier_journeys.get().set_visible(true);
                        journeys_view.show_toast("Error getting earlier Journeys");
                    },
                },
                Err(e) => {
                    error!("Handle error: {:#?}", e);
                    journeys_view.imp().journeys_search_spinner.get().set_visible(false);
                    journeys_view.imp().earlier_journeys_spinner.get().set_visible(true);
                    journeys_view.show_toast("internal Error getting earlier Journeys");
                },
            };
        }));
    }

    pub fn later_journeys(&self) {
        self.imp().later_journeys_spinner.get().set_visible(true);
        self.imp().later_journeys.get().set_visible(false);

        let hafas_endpoint = &*self.imp().endpoint.borrow();
        let endpoint = hafas_endpoint.get().clone();

        let journeys_handle: tokio::task::JoinHandle<Result<Journeys, BoxError>> = crate::RUNTIME
            .spawn(async move {
                let journeys = endpoint.get_later_journeys().await;
                // std::thread::sleep(std::time::Duration::from_secs(2));
                journeys
            });

        gtk_macros::spawn!(clone!(
        @weak self as journeys_view
        => async move {
            match journeys_handle.await {
                Ok(journeys_handle_inner) => match journeys_handle_inner {
                    Ok(journeys) => {
                        journeys_view.imp().later_journeys.get().set_visible(true);
                        journeys_view.imp().later_journeys_spinner.get().set_visible(false);
                        for journey in journeys.journeys.iter() {
                            let row = JourneyRow::new(&journey);
                            journeys_view.imp().journeys_listbox.get().append(&row);
                        }
                    },
                    Err(e) => {
                        warn!("Error getting later journeys: {:#?}", e);
                        journeys_view.imp().later_journeys_spinner.get().set_visible(false);
                        journeys_view.imp().later_journeys.get().set_visible(true);
                        journeys_view.show_toast("Error getting later Journeys");
                    },
                },
                Err(e) => {
                    error!("Handle error: {:#?}", e);
                    journeys_view.imp().journeys_search_spinner.get().set_visible(false);
                    journeys_view.imp().later_journeys_spinner.get().set_visible(true);
                    journeys_view.show_toast("internal Error getting later Journeys");
                },
            };
        }));
    }

    pub fn show_toast(&self, text: &str) {
        let _ = self.activate_action("win.add-toast", Some(&gettext(text).to_variant()));
    }

    pub fn clear_journeys_list(&self) {
        // TODO: move to utils and use to clear location list as well
        loop {
            let row = match self.imp().journeys_listbox.get().row_at_index(0) {
                Some(r) => r,
                None => break,
            };
            self.imp().journeys_listbox.get().remove(&row);
        }
    }
}
