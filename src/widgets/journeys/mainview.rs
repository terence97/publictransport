use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

mod imp {
    use super::*;
    use crate::models::endpoint::HafasEndpoint;
    use crate::models::location_id::LocationId;
    use glib::{BindingFlags, ParamFlags, ParamSpec, ParamSpecObject, Value};
    use once_cell::sync::Lazy;
    use std::cell::RefCell;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/journeys_mainview.ui")]
    pub struct JourneysMainView {
        // Template widgets
        #[template_child]
        pub departure_place: TemplateChild<crate::widgets::locationsearch::LocationSearch>,
        #[template_child]
        pub arrival_place: TemplateChild<crate::widgets::locationsearch::LocationSearch>,
        #[template_child]
        pub journeys_view: TemplateChild<crate::widgets::journeys::journeys_view::JourneysView>,
        #[template_child]
        pub date_time: TemplateChild<crate::widgets::datetime::button::DatetimeButton>,
        #[template_child]
        pub refresh_journeys: TemplateChild<gtk::Button>,
        #[template_child]
        pub transportation_means: TemplateChild<gtk::Button>,
        #[template_child]
        pub transportation_means_image: TemplateChild<gtk::Image>,
        pub products_used_popover: crate::widgets::products_used_popover::ProductsUsed,
        pub endpoint: RefCell<HafasEndpoint>,
        pub departure_id: RefCell<LocationId>,
        pub arrival_id: RefCell<LocationId>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for JourneysMainView {
        const NAME: &'static str = "PublictransportJourneysMainView";
        type Type = super::JourneysMainView;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);

            klass.install_action("win.show-journeys-refresh", None, |obj, _, _| {
                obj.imp().refresh_journeys.get().set_visible(true);
            });

            klass.install_action("win.hide-journeys-refresh", None, |obj, _, _| {
                obj.imp().refresh_journeys.get().set_visible(false);
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for JourneysMainView {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::new(
                        // Name
                        "endpoint",
                        // Nickname
                        "endpoint",
                        // Short description
                        "endpoint",
                        // Default value
                        HafasEndpoint::static_type(),
                        // The property can be read and written to
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecObject::new(
                        // Name
                        "departure-id",
                        // Nickname
                        "departure-id",
                        // Short description
                        "departure-id",
                        // Default value
                        LocationId::static_type(),
                        // The property can be read and written to
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecObject::new(
                        // Name
                        "arrival-id",
                        // Nickname
                        "arrival-id",
                        // Short description
                        "arrival-id",
                        // Default value
                        LocationId::static_type(),
                        // The property can be read and written to
                        ParamFlags::READWRITE,
                    ),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _obj: &Self::Type, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "endpoint" => {
                    let endpoint = value
                        .get()
                        .expect("The value needs to be of type `HafasEndpoint`.");
                    self.endpoint.replace(endpoint);
                }
                "departure-id" => {
                    let departure_id = value
                        .get()
                        .expect("The value needs to be of type `LocationId`.");
                    self.departure_id.replace(departure_id);
                }
                "arrival-id" => {
                    let arrival_id = value
                        .get()
                        .expect("The value needs to be of type `LocationId`.");
                    self.arrival_id.replace(arrival_id);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "endpoint" => self.endpoint.borrow().to_value(),
                "departure-id" => self.departure_id.borrow().to_value(),
                "arrival-id" => self.arrival_id.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            obj.bind_property("endpoint", &obj.imp().journeys_view.get(), "endpoint")
                .flags(BindingFlags::DEFAULT)
                .build();

            obj.bind_property("endpoint", &obj.imp().products_used_popover, "endpoint")
                .flags(BindingFlags::BIDIRECTIONAL)
                .build();

            obj.imp()
                .arrival_place
                .get()
                .imp()
                .popover
                .bind_property("selected-location-id", obj, "arrival-id")
                .flags(BindingFlags::DEFAULT)
                .build();

            obj.imp()
                .departure_place
                .get()
                .imp()
                .popover
                .bind_property("selected-location-id", obj, "departure-id")
                .flags(glib::BindingFlags::DEFAULT)
                .build();

            obj.connect_notify_local(Some("departure-id"), move |journeys_main_view, _| {
                let departure_id = journeys_main_view
                    .property::<LocationId>("departure-id")
                    .get();
                let hafas_endpoint = journeys_main_view
                    .property::<HafasEndpoint>("endpoint")
                    .get();
                let hafas_endpoint_new =
                    HafasEndpoint::new(hafas_endpoint.set_departure_id(departure_id));
                journeys_main_view.set_property("endpoint", hafas_endpoint_new);
            });

            obj.connect_notify_local(Some("arrival-id"), move |journeys_main_view, _| {
                let arrival_id = journeys_main_view
                    .property::<LocationId>("arrival-id")
                    .get();
                let hafas_endpoint = journeys_main_view
                    .property::<HafasEndpoint>("endpoint")
                    .get();
                let hafas_endpoint_new =
                    HafasEndpoint::new(hafas_endpoint.set_arrival_id(arrival_id));
                journeys_main_view.set_property("endpoint", hafas_endpoint_new);
            });

            obj.connect_notify_local(Some("endpoint"), move |journeys_main_view, _| {
                let endpoint = journeys_main_view
                    .property::<HafasEndpoint>("endpoint")
                    .get();
                let products_used = endpoint.journeys_config.products_used.to_vec();

                for product_used in products_used.iter() {
                    let product_used_inner = *product_used.used.lock().unwrap();
                    if product_used_inner == false {
                        journeys_main_view
                            .imp()
                            .transportation_means_image
                            .set_icon_name(Some("bus-symbolic-circle"));
                        break;
                    }
                    journeys_main_view
                        .imp()
                        .transportation_means_image
                        .set_icon_name(Some("bus-symbolic"));
                }
            });

            let journeys_view = self.journeys_view.clone();
            self.refresh_journeys.connect_clicked(move |_| {
                journeys_view.journeys();
            });

            self.products_used_popover
                .set_parent(&self.transportation_means.get());
            let products_used_popover = self.products_used_popover.clone();
            self.transportation_means.connect_clicked(move |_| {
                products_used_popover.popup();
            });
        }
    }
    impl WidgetImpl for JourneysMainView {}
    impl BoxImpl for JourneysMainView {}
}

glib::wrapper! {
    pub struct JourneysMainView(ObjectSubclass<imp::JourneysMainView>)
        @extends gtk::Widget, gtk::Box,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Orientable;
}

impl JourneysMainView {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create JourneysMainView")
    }
}
