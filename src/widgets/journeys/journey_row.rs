use crate::models::journey::HafasJourney;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};
use hafas_client::Journey;

mod imp {
    use super::*;
    use std::cell::RefCell;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/journeys_journey_row.ui")]
    pub struct JourneyRow {
        #[template_child]
        pub origin: TemplateChild<gtk::Label>,
        #[template_child]
        pub planned_departure: TemplateChild<gtk::Label>,
        #[template_child]
        pub destination: TemplateChild<gtk::Label>,
        #[template_child]
        pub planned_arrival: TemplateChild<gtk::Label>,
        #[template_child]
        pub duration: TemplateChild<gtk::Label>,
        #[template_child]
        pub departure_delay: TemplateChild<gtk::Label>,
        #[template_child]
        pub arrival_delay: TemplateChild<gtk::Label>,
        #[template_child]
        pub lines: TemplateChild<gtk::FlowBox>,
        #[template_child]
        pub warning_badge: TemplateChild<gtk::Image>,
        pub journey: RefCell<HafasJourney>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for JourneyRow {
        const NAME: &'static str = "JourneyRow";
        type Type = super::JourneyRow;
        type ParentType = gtk::ListBoxRow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for JourneyRow {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);
        }
    }
    impl WidgetImpl for JourneyRow {}
    impl ListBoxRowImpl for JourneyRow {}
}

glib::wrapper! {
    pub struct JourneyRow(ObjectSubclass<imp::JourneyRow>)
    @extends gtk::Widget, gtk::ListBoxRow,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Actionable;
}

impl JourneyRow {
    pub fn new(journey: &Journey) -> Self {
        let journey_row: JourneyRow = glib::Object::new(&[]).expect("Failed to create JourneyRow");
        *journey_row.imp().journey.borrow_mut() = HafasJourney::new(&journey.clone());
        journey_row.fill();
        journey_row
    }

    pub fn fill(&self) -> &Self {
        let journey = self.imp().journey.borrow().get().unwrap();
        let planned_departure = &journey.legs[0].plannedDeparture.unwrap();
        let departure_delay = match &journey.legs[0].departure {
            Some(d) => {
                let mut departure_delay = String::from("+");
                departure_delay.push_str(
                    &d.signed_duration_since(*planned_departure)
                        .num_minutes()
                        .to_string(),
                );
                departure_delay
            }
            None => String::from(""),
        };

        let planned_arrival = &journey
            .legs
            .last()
            .expect("Connection part not found")
            .plannedArrival
            .unwrap();
        let arrival_delay = match &journey
            .legs
            .last()
            .expect("Connection part not found")
            .arrival
        {
            Some(d) => {
                let mut arrival_delay = String::from("+");
                arrival_delay.push_str(
                    &d.signed_duration_since(*planned_arrival)
                        .num_minutes()
                        .to_string(),
                );
                arrival_delay
            }
            None => String::from(""),
        };

        let duration = match &journey.legs[0].plannedDeparture {
            Some(depa) => {
                let duration = match &journey
                    .legs
                    .last()
                    .expect("Connection part not found")
                    .plannedArrival
                {
                    Some(arri) => {
                        let duration = arri.signed_duration_since(*depa);
                        let minutes = duration.num_minutes() % 60;
                        let mut duration_str = String::from(" ");
                        duration_str.push_str(&duration.num_hours().to_string());
                        duration_str.push(':');
                        if minutes < 10 {
                            duration_str.push('0');
                        }
                        duration_str.push_str(&minutes.to_string());
                        duration_str.push_str(&" h".to_string());
                        duration_str
                    }
                    _ => "".to_string(),
                };
                duration
            }
            _ => "".to_string(),
        };

        for l in journey.legs.iter() {
            self.add_line_box(&l.line);
        }

        let mut warning = false;
        let mut i = 0;
        for j in journey.legs.iter() {
            // don't show warning if train is late at origin or destination (can be seen in the overview anyway)
            match j.departure {
                Some(d) => {
                    if j.plannedDeparture.unwrap() != d {
                        if i != 0 {
                            warning = true;
                        }
                    }
                }
                _ => (),
            }
            match j.arrival {
                Some(a) => {
                    if j.plannedArrival.unwrap() != a {
                        if i != journey.legs.len() - 1 {
                            warning = true;
                        }
                    }
                }
                _ => (),
            }
            i += 1;
        }

        let destination = journey
            .legs
            .last()
            .expect("Connection part not found")
            .destination
            .name
            .clone();

        self.imp()
            .origin
            .get()
            .set_markup(&journey.legs[0].origin.name.clone());
        self.imp()
            .planned_departure
            .get()
            .set_markup(&planned_departure.format("%H:%M").to_string());
        self.imp()
            .destination
            .get()
            .set_markup(&destination.clone());
        self.imp()
            .planned_arrival
            .get()
            .set_markup(&planned_arrival.format("%H:%M").to_string());
        self.imp().duration.get().set_markup(&duration);
        self.imp()
            .departure_delay
            .get()
            .set_markup(&departure_delay);
        self.imp().arrival_delay.get().set_markup(&arrival_delay);
        self.imp().warning_badge.get().set_visible(warning);

        self
    }

    pub fn add_line_box(&self, line: &Option<String>) {
        let line_box = gtk::Box::new(gtk::Orientation::Horizontal, 0);
        line_box.set_halign(gtk::Align::Center);
        match line {
            Some(l) => {
                let mut line_str = " ".to_string();
                line_str.push_str(&l);
                line_str.push_str(&" ".to_string());

                line_box.append(&gtk::Label::new(Some(&line_str.to_string())));
            }
            None => {
                let image = gtk::Image::new();
                image.set_icon_name(Some("walking-symbolic"));
                line_box.append(&image);
            }
        }

        let dark = adw::StyleManager::default().is_dark();
        if dark {
            line_box.add_css_class("product-background-dark")
        } else {
            line_box.add_css_class("product-background")
        }

        self.imp().lines.get().append(&line_box);
    }
}
