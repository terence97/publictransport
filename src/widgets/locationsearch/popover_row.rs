use glib::{ParamFlags, ParamSpec, ParamSpecString, Value};
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use once_cell::sync::Lazy;
use std::cell::RefCell;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct LocationPopoverRow {
        pub location_id: RefCell<String>,
        pub location_name: RefCell<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for LocationPopoverRow {
        const NAME: &'static str = "LocationPopoverRow";
        type Type = super::LocationPopoverRow;
        type ParentType = gtk::Box;
    }

    impl ObjectImpl for LocationPopoverRow {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::new(
                        // Name
                        "location-id-row",
                        // Nickname
                        "location-id-row",
                        // Short description
                        "location-id-row",
                        // Default value
                        None,
                        // The property can be read and written to
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecString::new(
                        // Name
                        "location-name-row",
                        // Nickname
                        "location-name-row",
                        // Short description
                        "location-name-row",
                        // Default value
                        None,
                        // The property can be read and written to
                        ParamFlags::READWRITE,
                    ),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _obj: &Self::Type, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "location-id-row" => {
                    let location_id = value
                        .get()
                        .expect("The value needs to be of type `String`.");
                    self.location_id.replace(location_id);
                }
                "location-name-row" => {
                    let location_name = value
                        .get()
                        .expect("The value needs to be of type `String`.");
                    self.location_name.replace(location_name);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "location-id-row" => self.location_id.borrow().to_value(),
                "location-name-row" => self.location_name.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            // obj.new_location_row();
        }
    }
    impl WidgetImpl for LocationPopoverRow {}
    impl BoxImpl for LocationPopoverRow {}
}

glib::wrapper! {
    pub struct LocationPopoverRow(ObjectSubclass<imp::LocationPopoverRow>)
        @extends gtk::Widget, gtk::Box,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Orientable;
}

impl LocationPopoverRow {
    pub fn new(location_id: &String, location_name: &String) -> Self {
        let row: LocationPopoverRow = glib::Object::new(&[
            ("location-id-row", location_id),
            ("location-name-row", location_name),
        ])
        .expect("Failed to create LocationPopoverRow");
        row.new_location_row(); // not done in constructed because property location-id-row isn't set then
        row
    }

    pub fn new_location_row(&self) -> &Self {
        self.set_can_focus(false);
        self.set_visible(true);
        self.set_margin_start(5);
        self.set_margin_end(5);
        self.set_margin_top(5);
        self.set_margin_bottom(5);
        self.append(&gtk::Label::new(Some(
            &self.property::<String>("location-name-row"),
        )));
        self
    }
}
