use crate::models::location_id::LocationId;
use crate::widgets::locationsearch::LocationPopoverRow;
use glib::clone;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};
use hafas_client::{BoxError, Place};
use log::{error, warn};

mod imp {
    use super::*;
    use crate::models::endpoint::HafasEndpoint;
    use glib::{ParamFlags, ParamSpec, ParamSpecObject, Value};
    use once_cell::sync::Lazy;
    use std::cell::RefCell;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/locationsearch_popover.ui")]
    pub struct LocationPopover {
        // Template widgets
        #[template_child]
        pub station_search_spinner: TemplateChild<gtk::Box>,
        #[template_child]
        pub no_locations_box: TemplateChild<gtk::Box>,
        #[template_child]
        pub autocomplete_listbox: TemplateChild<gtk::ListBox>,
        pub endpoint: RefCell<HafasEndpoint>,
        pub selected_location_id: RefCell<LocationId>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for LocationPopover {
        const NAME: &'static str = "PublictransportLocationPopover";
        type Type = super::LocationPopover;
        type ParentType = gtk::Popover;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for LocationPopover {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::new(
                        // Name
                        "selected-location-id",
                        // Nickname
                        "selected-location-id",
                        // Short description
                        "selected-location-id",
                        // Default value
                        LocationId::static_type(),
                        // The property can be read and written to
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecObject::new(
                        // Name
                        "endpoint",
                        // Nickname
                        "endpoint",
                        // Short description
                        "endpoint",
                        // Default value
                        HafasEndpoint::static_type(),
                        // The property can be read and written to
                        ParamFlags::READWRITE,
                    ),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _obj: &Self::Type, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "endpoint" => {
                    let endpoint = value
                        .get()
                        .expect("The value needs to be of type `HafasEndpoint`.");
                    self.endpoint.replace(endpoint);
                }
                "selected-location-id" => {
                    let selected_location_id = value
                        .get()
                        .expect("The value needs to be of type `String`.");
                    self.selected_location_id.replace(selected_location_id);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "endpoint" => self.endpoint.borrow().to_value(),
                "selected-location-id" => self.selected_location_id.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            obj.setup_signal();
        }
    }
    impl WidgetImpl for LocationPopover {}
    impl PopoverImpl for LocationPopover {}
}

glib::wrapper! {
    pub struct LocationPopover(ObjectSubclass<imp::LocationPopover>)
    @extends gtk::Widget, gtk::Popover,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Native, gtk::ShortcutManager;
}

impl LocationPopover {
    pub fn new(parent: &impl IsA<gtk::Widget>) -> Self {
        let location_popover: LocationPopover =
            glib::Object::new(&[]).expect("Failed to create LocationPopover");
        location_popover.set_parent(parent);

        location_popover
    }

    pub fn locations(&self, search_string: String) {
        self.show_search_spinner();

        let hafas_endpoint = &*self.imp().endpoint.borrow();
        let endpoint = hafas_endpoint.get();

        let locations_handle: tokio::task::JoinHandle<Result<Vec<Place>, BoxError>> =
            crate::RUNTIME.spawn(async move {
                let locations = endpoint.get_locations(search_string).await;
                // std::thread::sleep(std::time::Duration::from_secs(2));
                locations
            });

        gtk_macros::spawn!(clone!(
        @weak self as popover
        => async move {
            match locations_handle.await {
                Ok(locations_handle_inner) => match locations_handle_inner {
                    Ok(locations) => {
                        popover.clear_location_list();
                        for location in locations.iter() {
                            let row = LocationPopoverRow::new(&location.id, &location.name);
                            popover.imp().autocomplete_listbox.get().append(&row);
                        }
                        popover.show_locations();
                    },
                    Err(e) => {
                        popover.show_error();
                        warn!("Error getting locations: {:#?}", e);
                    },
                },
                Err(e) => {
                    popover.show_error();
                    error!("Handle error: {:#?}", e);
                },
            };
        }));
    }

    pub fn setup_signal(&self) {
        self.imp().autocomplete_listbox.get().connect_row_selected(clone!(@weak self as popover => move |_ , row_selected| {
                match row_selected {
                    Some(r) => {
                        let location_id_row = r.child().unwrap().property::<String>("location-id-row");
                        let location_id = LocationId::new(Some(location_id_row.parse::<i64>().unwrap()));
                        popover.set_property("selected-location-id", location_id);
                        popover.parent().unwrap().set_property("text", &r.child().unwrap().property::<String>("location-name-row"));                 
                        popover.parent().unwrap().parent().unwrap().set_property("location-id-set", true);
                    },
                    None => (),
                }
                popover.popdown();
            }));
    }

    pub fn show_search_spinner(&self) {
        self.imp().station_search_spinner.get().set_visible(true);
        self.clear_location_list();
        self.imp().no_locations_box.get().set_visible(false);
        self.popup();
    }

    pub fn show_locations(&self) {
        self.imp().station_search_spinner.get().set_visible(false);
        self.imp().no_locations_box.set_visible(false);
        self.imp().autocomplete_listbox.get().set_visible(true);
    }

    pub fn show_error(&self) {
        self.imp().station_search_spinner.get().set_visible(false);
        self.imp().no_locations_box.set_visible(true);
    }

    pub fn clear_location_list(&self) {
        loop {
            let row = match self.imp().autocomplete_listbox.get().row_at_index(0) {
                Some(r) => r,
                None => break,
            };
            self.imp().autocomplete_listbox.get().remove(&row);
        }
    }
}

impl std::default::Default for LocationPopover {
    fn default() -> Self {
        glib::Object::new(&[]).expect("Failed to create LocationPopover")
    }
}
