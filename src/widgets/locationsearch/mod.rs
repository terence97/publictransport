pub mod popover;
pub mod popover_row;

use self::popover_row::LocationPopoverRow;

use glib::clone;
use glib::value::Value;
use glib::{BindingFlags, ParamFlags, ParamSpec, ParamSpecBoolean, ParamSpecString};
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};
use once_cell::sync::Lazy;
use std::cell::{Cell, RefCell};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/locationsearch.ui")]
    pub struct LocationSearch {
        #[template_child]
        pub location_search: TemplateChild<gtk::SearchEntry>,
        pub popover: popover::LocationPopover,
        pub location_id: RefCell<String>,
        pub location_id_set: Cell<bool>,
        pub placeholder_text: RefCell<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for LocationSearch {
        const NAME: &'static str = "PublictransportLocationSearch";
        type Type = super::LocationSearch;
        type ParentType = gtk::Box; // SearchEntry is not subclassable, so we put it in a box

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for LocationSearch {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::new(
                        // Name
                        "placeholder-text",
                        // Nickname
                        "placeholder-text",
                        // Short description
                        "placeholder-text",
                        // Default value
                        Some("from.."),
                        // The property can be read and written to
                        ParamFlags::READWRITE,
                    ),
                    ParamSpecBoolean::new(
                        // Name
                        "location-id-set",
                        // Nickname
                        "location-id-set",
                        // Short description
                        "location-id-set",
                        // Default value
                        false,
                        // The property can be read and written to
                        ParamFlags::READWRITE,
                    ),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _obj: &Self::Type, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "placeholder-text" => {
                    let input_placeholder_text = value
                        .get()
                        .expect("The value needs to be of type `String`.");
                    self.placeholder_text.replace(input_placeholder_text);
                }
                "location-id-set" => {
                    let location_id_set = value
                        .get()
                        .expect("The value needs to be of type `Boolean`.");
                    self.location_id_set.replace(location_id_set);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "placeholder-text" => self.placeholder_text.borrow().to_value(),
                "location-id-set" => self.location_id_set.get().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            obj.bind_property(
                "placeholder-text",
                &obj.imp().location_search.get(),
                "placeholder-text",
            )
            .flags(BindingFlags::SYNC_CREATE)
            .build();

            obj.setup_popover();
        }
    }
    impl WidgetImpl for LocationSearch {}
    impl BoxImpl for LocationSearch {}
}

glib::wrapper! {
    pub struct LocationSearch(ObjectSubclass<imp::LocationSearch>)
        @extends gtk::Widget,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Editable;
}

impl LocationSearch {
    pub fn new() -> Self {
        let location_search: LocationSearch =
            glib::Object::new(&[]).expect("Failed to create LocationSearch");

        location_search.setup_popover();

        location_search
    }

    pub fn setup_popover(&self) {
        let location_search = self.imp().location_search.get();
        self.imp().popover.set_parent(&location_search);

        location_search.connect_search_changed(
            clone!(@weak self as location_search => move |location_search_text| {
                let location_search_text_str = location_search_text.text().as_str().to_string();
                if &location_search_text_str.len() > &2 && location_search.imp().location_id_set.get() == false {
                    location_search.imp().popover.locations(location_search_text_str);
                } else if &location_search_text_str.len() > &2 && location_search.imp().location_id_set.get() {
                    location_search.imp().popover.popdown();
                    location_search.imp().location_id_set.set(false);
                } else {
                    location_search.imp().popover.popdown();
                }
            }),
        );
    }
}
