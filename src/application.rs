use glib::clone;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib};

use crate::config;
use crate::PublictransportWindow;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct PublictransportApplication {}

    #[glib::object_subclass]
    impl ObjectSubclass for PublictransportApplication {
        const NAME: &'static str = "PublictransportApplication";
        type Type = super::PublictransportApplication;
        type ParentType = gtk::Application;
    }

    impl ObjectImpl for PublictransportApplication {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            obj.setup_gactions();
            obj.set_accels_for_action("app.quit", &["<primary>q"]);
            obj.set_accels_for_action("win.show-help-overlay", &["<primary>question"]);

            // register custom subclasses
            crate::widgets::locationsearch::LocationSearch::static_type();
            crate::widgets::journeys::mainview::JourneysMainView::static_type();
            crate::widgets::journeys::journeys_view::JourneysView::static_type();
            crate::widgets::journeys::detailsview::JourneysDetailsView::static_type();
            crate::widgets::datetime::button::DatetimeButton::static_type();
            crate::widgets::datetime::view::DateTimeView::static_type();
        }
    }

    impl ApplicationImpl for PublictransportApplication {
        // We connect to the activate callback to create a window when the application
        // has been launched. Additionally, this callback notifies us when the user
        // tries to launch a "second instance" of the application. When they try
        // to do that, we'll just present any existing window.
        fn activate(&self, application: &Self::Type) {
            // Get the current window or create one if necessary
            let window = if let Some(window) = application.active_window() {
                window
            } else {
                let window = PublictransportWindow::new(application);
                window.set_default_size(400, 700);
                window.upcast()
            };

            // Ask the window manager/compositor to present the window
            window.present();
        }

        fn startup(&self, app: &Self::Type) {
            self.parent_startup(app);

            adw::functions::init();

            if let Some(ref display) = gtk::gdk::Display::default() {
                let p = gtk::CssProvider::new();
                gtk::CssProvider::load_from_resource(
                    &p,
                    "/com/gitlab/maevemi/publictransport/style.css",
                );
                gtk::StyleContext::add_provider_for_display(display, &p, 5);
                let theme = gtk::IconTheme::for_display(display);
                theme.add_resource_path("/com/gitlab/maevemi/publictransport/icons/");
            }
        }
    }

    impl GtkApplicationImpl for PublictransportApplication {}
}

glib::wrapper! {
    pub struct PublictransportApplication(ObjectSubclass<imp::PublictransportApplication>)
    @extends gio::Application, gtk::Application,
    @implements gio::ActionGroup, gio::ActionMap;
}

impl PublictransportApplication {
    pub fn new(application_id: &str, flags: &gio::ApplicationFlags) -> Self {
        glib::Object::new(&[("application-id", &application_id), ("flags", flags)])
            .expect("Failed to create PublictransportApplication")
    }

    fn setup_gactions(&self) {
        let quit_action = gio::SimpleAction::new("quit", None);
        quit_action.connect_activate(clone!(@weak self as app => move |_, _| {
            app.quit();
        }));
        self.add_action(&quit_action);

        let about_action = gio::SimpleAction::new("about", None);
        about_action.connect_activate(clone!(@weak self as app => move |_, _| {
            app.show_about();
        }));
        self.add_action(&about_action);
    }

    fn show_about(&self) {
        let window = self.active_window().unwrap();
        let dialog = gtk::builders::AboutDialogBuilder::new()
            .transient_for(&window)
            .modal(true)
            .program_name("Public Transport")
            .version(config::VERSION)
            .authors(vec!["Maeve".into()])
            .logo_icon_name(config::APP_ID)
            .build();

        dialog.present();
    }
}
